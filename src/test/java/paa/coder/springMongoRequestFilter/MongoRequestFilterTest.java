package paa.coder.springMongoRequestFilter;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import paa.coder.springMongoRequestFilter.domains.CompanySize;
import paa.coder.springMongoRequestFilter.payloads.RestFilter;
import paa.coder.springMongoRequestFilter.payloads.RestOrder;
import paa.coder.springMongoRequestFilter.payloads.RestPage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MongoDbUnitExtension.class)
class MongoRequestFilterTest {

    @AfterEach
    public void clear() throws Exception{
        MongoTemplate t = MongoDbUnitExtension.mangoCfg.mongoTemplate();
        t.getCollectionNames().forEach(t::dropCollection);
    }

    @Test
    void whenFilterAndAdditionalIsEmpty() throws Exception{
        MongoTemplate t = MongoDbUnitExtension.mangoCfg.mongoTemplate();
        assertThat(t.find(new Query(), CompanySize.class)).hasSize(0);
        CompanySize companySize = new CompanySize();
        companySize.setCount(100L);
        companySize.setLength(100L);
        companySize = t.save(companySize);

        MongoRequestFilter f = new MongoRequestFilter(new RestFilter(), t);

        List<CompanySize> l = f.find(CompanySize.class);

        assertThat(l).hasSize(1).first().usingRecursiveComparison().isEqualTo(companySize);
    }

    @Test
    void whenFilterIsNotEmpty() throws Exception{
        MongoTemplate t = MongoDbUnitExtension.mangoCfg.mongoTemplate();
        assertThat(t.find(new Query(), CompanySize.class)).hasSize(0);
        t.save(new CompanySize(1L, 100L, 200L, null));
        CompanySize c2 = t.save(new CompanySize(2L, 200L, 200L, null));

        RestFilter restFilter = new RestFilter();
        restFilter.setQuery("{ companyId: 2 }");

        assertThat(new MongoRequestFilter(restFilter, t).find(CompanySize.class))
                .hasSize(1)
                .first()
                .usingRecursiveComparison()
                .isEqualTo(c2);
    }

    @Test
    void whenFilterAndAdditionalHasSameFilteredKey() throws Exception{

        MongoTemplate t = MongoDbUnitExtension.mangoCfg.mongoTemplate();
        assertThat(t.find(new Query(), CompanySize.class)).hasSize(0);

        t.save(new CompanySize(1L, 100L, 200L, null));
        CompanySize c2 = t.save(new CompanySize(2L, 200L, 200L, null));

        RestFilter restFilter = new RestFilter();
        restFilter.setQuery("{ $or: [ { length: { $gt: 100 } }, { companyId: 1 } ] }");
        assertThat(new MongoRequestFilter(restFilter, t).find(CompanySize.class)).hasSize(2);

        MongoRequestFilter f = new MongoRequestFilter(restFilter, t);
        f.withCriteria(c -> c.and("companyId").is(2L));
        List<CompanySize> cizes = f.find(CompanySize.class);
        assertThat(cizes).hasSize(1).first().usingRecursiveComparison().isEqualTo(c2);
    }


    @Test
    void page() throws Exception{
        MongoTemplate t = MongoDbUnitExtension.mangoCfg.mongoTemplate();
        assertThat(t.find(new Query(), CompanySize.class)).hasSize(0);
        t.save(new CompanySize(3L, 100L, 200L, null));
        t.save(new CompanySize(4L, 5000L, 200L, null));
        t.save(new CompanySize(1L, 5000L, 200L, null));
        t.save(new CompanySize(2L, 5000L, 200L, null));
        t.save(new CompanySize(2L, 5000L, 200L, null));
        String id4 = t.save(new CompanySize(1L, 300L, 200L, null)).getId();
        String id3 = t.save(new CompanySize(2L, 800L, 200L, null)).getId();
        String id2 = t.save(new CompanySize(4L, 800L, 200L, null)).getId();
        String id1 = t.save(new CompanySize(2L, 1000L, 200L, null)).getId();


        MongoRequestFilter f = new MongoRequestFilter(new RestFilter(), t);
        f.getRestFilter().withPage(1).withPageCount(4).addOrder(new RestOrder(false, "length", "companyId"));

        RestPage<CompanySize> rp = f.page(CompanySize.class);
        assertThat(rp).isNotNull().hasFieldOrPropertyWithValue("totalElements", 9L).extracting(RestPage::getContent).asList().hasSize(4);

        assertThat(rp.getContent().get(0).getId()).isEqualTo(id1);
        assertThat(rp.getContent().get(1).getId()).isEqualTo(id2);
        assertThat(rp.getContent().get(2).getId()).isEqualTo(id3);
        assertThat(rp.getContent().get(3).getId()).isEqualTo(id4);

    }
}