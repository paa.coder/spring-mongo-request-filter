package paa.coder.springMongoRequestFilter;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import de.bwaldvogel.mongo.MongoServer;
import de.bwaldvogel.mongo.backend.memory.MemoryBackend;
import org.junit.jupiter.api.extension.AfterAllCallback;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

import java.net.InetSocketAddress;

public class MongoDbUnitExtension implements BeforeAllCallback, AfterAllCallback {

    private MongoClient client;
    private MongoServer server;
    public static AbstractMongoConfiguration mangoCfg;

    @Override
    public void beforeAll(ExtensionContext extensionContext) throws Exception{

        server = new MongoServer(new MemoryBackend());
        InetSocketAddress serverAddress = server.bind();

        client = new MongoClient(new ServerAddress(serverAddress));
        mangoCfg = new AbstractMongoConfiguration() {
            @Override
            public MongoClient mongoClient(){
                return client;
            }

            @Override
            protected String getDatabaseName(){
                return "test";
            }
        };

    }

    @Override
    public void afterAll(ExtensionContext extensionContext) throws Exception{
        client.close();
        server.shutdown();
        mangoCfg=null;
    }
}
