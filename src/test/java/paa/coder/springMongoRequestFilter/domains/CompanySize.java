package paa.coder.springMongoRequestFilter.domains;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Data
@Document("companySizes")
@NoArgsConstructor
public class CompanySize{

    @Id
    private String id;

    private Long companyId;
    private Long length = 0L;
    private Long count = 0L;
    private Instant date;

    @Indexed(name = "expire_after_seconds_index",expireAfterSeconds = 1)
    private Instant createdAt = null;

    public CompanySize(Long companyId, Long length, Long count, Instant date){
        this.companyId = companyId;
        this.length = length;
        this.count = count;
        this.date = date;
    }
}
