package paa.coder.springMongoRequestFilter.payloads;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.Sort;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class RestOrder {
    private List<String> fields = new ArrayList<>();
    private boolean isAsc = true;

    @JsonCreator
    public RestOrder(@JsonProperty("fields")List<String> fields,@JsonProperty("isAsc") boolean isAsc){
        this.fields.addAll(fields);
        this.isAsc = isAsc;
    }

    public RestOrder(boolean isAsc, String ... fields){
        this.fields.addAll(Arrays.asList(fields));
        this.isAsc = isAsc;
    }

    public RestOrder(String ... fields){
        this.fields.addAll(Arrays.asList(fields));
    }

    public RestOrder(String field, boolean isAsc){
        this.isAsc = isAsc;
        this.fields.add(field);
    }

    public RestOrder(String field){
        this.fields.add(field);
    }


    public RestOrder(ArrayList<String> fields){
        this.fields = fields;
    }

    public List<Sort.Order> getSortOrder(){
        return fields.stream().map(i->new Sort.Order(isAsc?Sort.Direction.ASC:Sort.Direction.DESC,i)).collect(Collectors.toList());
    }
}
