package paa.coder.springMongoRequestFilter.payloads;

import lombok.Data;
import org.bson.Document;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Data
public class RestFilter {
    private String query;
    private List<RestOrder> order = new ArrayList<>();
    private Integer page;
    private Integer pageCount;
    private String hash;

    public RestFilter addOrder(RestOrder... orders){
        this.order.addAll(Arrays.asList(orders));
        return this;
    }


    public RestFilter withPage(Integer page){
        this.page = page;
        return this;
    }

    public RestFilter withPageCount(Integer pageCount){
        this.pageCount = pageCount;
        return this;
    }

    public Optional<Criteria> criteriaDefinition(){
        return Optional.ofNullable(query).map(i->new Criteria().andOperator(new CustomCriteria(i)));
    }

    private static class CustomCriteria extends Criteria{

        private final Document document;

        public CustomCriteria(String str){
            this.document = Document.parse(str);
        }

        @Override
        public Document getCriteriaObject(){
            return document;
        }
    }
}
