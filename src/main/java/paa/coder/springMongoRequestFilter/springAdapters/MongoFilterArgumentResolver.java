package paa.coder.springMongoRequestFilter.springAdapters;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;
import paa.coder.springMongoRequestFilter.MongoRequestFilter;
import paa.coder.springMongoRequestFilter.payloads.RestFilter;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class MongoFilterArgumentResolver implements HandlerMethodArgumentResolver {

    public static final String ATTRIBUTE_FILTER = "custom_storage_filter_request_body";
    private final ObjectMapper objectMapper;
    private final MongoOperations mongoOperations;

    public MongoFilterArgumentResolver(ObjectMapper objectMapper, MongoOperations mongoOperations){
        this.objectMapper = objectMapper;
        this.mongoOperations = mongoOperations;
    }

    @Override
    public boolean supportsParameter(MethodParameter methodParameter){
        return methodParameter.getParameterAnnotation(MongoFilterProperty.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest,
                                  WebDataBinderFactory webDataBinderFactory){


        RestFilter restFilter = new RestFilter();
        HttpServletRequest r = nativeWebRequest.getNativeRequest(HttpServletRequest.class);
        if(r == null){
            return new MongoRequestFilter(restFilter, mongoOperations);
        }
        Object body = r.getAttribute(ATTRIBUTE_FILTER);
        String json = "";
        try{
            if(body!=null){
                json = IOUtils.toString((byte[])body, r.getCharacterEncoding());
            }else{
                json = IOUtils.toString(r.getInputStream(), r.getCharacterEncoding());
            }
        }catch(IOException ignore){
        }
        try{
            MongoFilterProperty af = methodParameter.getParameterAnnotation(MongoFilterProperty.class);
            if(!json.isBlank() && af != null){
                if(af.value().isBlank()){
                    restFilter = objectMapper.readValue(json, RestFilter.class);
                }else{
                    Map<String,Object> map = objectMapper.readValue(json, new TypeReference<HashMap<String,Object>>() {
                    });
                    restFilter = Optional
                            .ofNullable(map.get(af.value()))
                            .map(i ->objectMapper.convertValue(i, RestFilter.class))
                            .orElse(new RestFilter());
                }

            }
        }catch(IOException ignore){
            ignore.printStackTrace();
        }


        return new MongoRequestFilter(restFilter, mongoOperations);
    }

}
