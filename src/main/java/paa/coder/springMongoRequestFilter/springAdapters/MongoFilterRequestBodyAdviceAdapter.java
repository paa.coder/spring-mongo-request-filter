package paa.coder.springMongoRequestFilter.springAdapters;

import org.apache.commons.io.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.mvc.method.annotation.RequestBodyAdviceAdapter;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Optional;

@ControllerAdvice
public class MongoFilterRequestBodyAdviceAdapter extends RequestBodyAdviceAdapter {

    @Override
    public boolean supports(MethodParameter methodParameter, Type type, Class<? extends HttpMessageConverter<?>> aClass){
        return methodParameter.hasMethodAnnotation(MongoFilterMapping.class);
    }

    @Override
    public HttpInputMessage beforeBodyRead(HttpInputMessage inputMessage, MethodParameter parameter, Type targetType,
                                           Class<? extends HttpMessageConverter<?>> converterType) throws IOException{

        if(Optional.ofNullable(inputMessage.getHeaders().getContentType()).map(i->!i.includes(MediaType.APPLICATION_JSON)).orElse(true)){
            return super.beforeBodyRead(inputMessage,parameter,targetType,converterType);
        }
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        if(attributes==null || attributes.getAttribute(MongoFilterArgumentResolver.ATTRIBUTE_FILTER,0)!=null){
            return super.beforeBodyRead(inputMessage,parameter,targetType,converterType);
        }

        byte[] body = IOUtils.toByteArray(inputMessage.getBody());
        attributes.setAttribute(MongoFilterArgumentResolver.ATTRIBUTE_FILTER,body,0);
        return super.beforeBodyRead(new InputMessage(inputMessage.getHeaders(),body),parameter,targetType,converterType);
    }

    public static class InputMessage implements HttpInputMessage{
        private final byte[] body;
        private final HttpHeaders headers;


        public InputMessage(HttpHeaders headers, byte[] body){
            this.headers=headers;
            this.body=body;
        }

        @Override
        public InputStream getBody(){
            return new ByteArrayInputStream(body);
        }

        @Override
        public HttpHeaders getHeaders(){
            return headers;
        }
    }
}