package paa.coder.springMongoRequestFilter;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.util.CloseableIterator;
import paa.coder.springMongoRequestFilter.payloads.RestFilter;
import paa.coder.springMongoRequestFilter.payloads.RestPage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class MongoRequestFilter {

    private final RestFilter restFilter;
    private final MongoOperations mongoOperations;
    private Criteria criteria = new Criteria();

    public MongoRequestFilter(RestFilter restFilter, MongoOperations mongoOperations){
        this.restFilter = restFilter;
        this.mongoOperations = mongoOperations;
    }

    public RestFilter getRestFilter(){
        return restFilter;
    }

    public MongoRequestFilter withCriteria(Function<Criteria,Criteria> func){
        this.criteria = func.apply(criteria);
        return this;
    }

    public Criteria getCriteria(){
        return criteria;
    }

    private Criteria getMergedCriteria(){
        return getRestFilter().criteriaDefinition().map(i->new Criteria().andOperator(criteria,i)).orElse(criteria);
    }


    public <T> CloseableIterator<T> stream(Class<T> tClass){
        return mongoOperations.stream(new Query(getMergedCriteria()), tClass);
    }

    public <T> List<T> find(Class<T> tClass){
        return mongoOperations.find(new Query(getMergedCriteria()), tClass);
    }

    public <T> RestPage<T> page(Class<T> tClass){

        Long total = mongoOperations.count(new Query(getMergedCriteria()), tClass);
        if(total==0){
            return new RestPage<>(restFilter, new ArrayList<>(), total);
        }
        Sort sort = null;
        if(! restFilter.getOrder().isEmpty()){
            List<Sort.Order> sortOrder = restFilter.getOrder().stream().flatMap(i -> i.getSortOrder().stream()).collect(Collectors.toList());
            if(! sortOrder.isEmpty()){
                sort = Sort.by(sortOrder);
            }
        }
        PageRequest pr = Optional
                .ofNullable(restFilter.getPageCount())
                .map(pc -> Optional.ofNullable(restFilter.getPage()).map(p -> PageRequest.of(p, pc)).orElseGet(() -> PageRequest.of(0, pc)))
                .orElse(null);
        if(pr != null && sort != null){
            pr = PageRequest.of(pr.getPageNumber(), pr.getPageSize(), sort);
        }

        Query q = new Query(getMergedCriteria());
        if(pr != null){
            q = q.with(pr);
        }else if(sort != null){
            q = q.with(sort);
        }
        return new RestPage<>(restFilter, mongoOperations.find(q, tClass), total);

    }



}
